const express = require("express");
const request = require("request");
const dotenv = require("dotenv");
// const cron = require("node-cron");

dotenv.config();

const app = express();

const API_URL = "https://api.coingecko.com/api/v3/simple/price";
const TELEGRAM_CHANNEL_ID = process.env.TELEGRAM_CHANNEL_ID;
const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;

app.get("/", (req, res) => {
  request(
    `${API_URL}?ids=bitcoin&vs_currencies=usd`,
    (error, response, body) => {
      if (!error && response.statusCode === 200) {
        const btcPrice = JSON.parse(body)["bitcoin"]["usd"];
        sendPriceToTelegram(btcPrice);
        res.send("Sent BTC price to Telegram channel");
      } else {
        res.send("Error fetching BTC price");
      }
    }
  );
});

function sendPriceToTelegram(price) {
  const formattedPrice = price.toLocaleString(); // will format the price with a comma as the thousand separator
  request.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage`, {
      form: {
          chat_id: TELEGRAM_CHANNEL_ID,
          text: `Current BTC price is $${formattedPrice}`
      }
  });
}


// setInterval(() => {
//   app.get("/btc-price");
// }, 60000);

app.listen(3000, () => {
  console.log("Server running on port 3000");
});
